---
pagetitle: "Normal Area Between Problem"
params:
  mu: 23
  sigma: 8 
  x1: 10
  x2: 25
---

```{r child="question/NormalAreaBetweenQuestion.Rmd"}
```

**_Solution:_**  

```{r child="solution/NormalAreaBetweenSolution.Rmd"}
```
