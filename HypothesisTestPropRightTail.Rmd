---
pagetitle: "Hypothesis Testing - Proportions Right Tail Test Problem"
params:
  count: 101 
  n: 250 
  p: 0.34
---

```{r child="question/HypothesisTestPropRightTailQuestion.Rmd"}
```

**_Solution:_**  

```{r child="solution/HypothesisTestPropRightTailSolution.Rmd"}
```
